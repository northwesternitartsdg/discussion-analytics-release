<?php
/*
Copyright 2016 Northwestern University

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
*/
//ini_set('display_errors', 'On');
$config = parse_ini_file(dirname(__FILE__).'/../presto.ini', true);
$app_name = $config['app']['active'];
$app_config = parse_ini_file(dirname(__FILE__).'/../apps/'.$app_name.'/app.ini', true);

$phpjs = new StdClass();

$app = new StdClass();
$app->id = $app_name;
$app->mail = new StdClass();
$app->mail->address = $app_config['mail']['address'];

$mail = $app_config['pk-include']['mail'];
$json_cache = $app_config['pk-include']['json_cache'];
$auth = $app_config['pk-include']['auth'];
$oauth = $app_config['pk-include']['oauth'];
$throttle = $app_config['pk-include']['throttle'];
$canvas_files = $app_config['pk-include']['canvas_files'];
$phpjs->app_config = $app_config['app'];

require 'vendor/autoload.php';
$title = $app_config['app']['title'];
foreach (glob('apps/'.$app_name."/lib/*.php") as $filename) {
    require_once($filename);
}
if( $json_cache ) {
  require_once 'pk-lib/presto-json-cache.php';
}
if( $mail ) {
  require_once 'pk-lib/presto-mail.php';
}
if( $auth ) {
  require_once 'pk-lib/presto-auth.php';
}
if( $oauth ) {
  require_once 'pk-lib/presto-oauth.php';
}
if( $throttle ) {
  require_once 'pk-lib/presto-throttle.php';
}

?>
