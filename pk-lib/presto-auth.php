<?php
/*
Copyright 2016 Northwestern University

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
*/
require_once 'lti/blti.php';

$phpjs->credentials = new StdClass();
$devel = $_SERVER['HTTP_HOST'] == 'localhost'; //Bypass LTI auth
$soft_auth = false;


//Process LTI authentication
if( array_key_exists('post_oa', $_GET) && array_key_exists('pk_credentials', $_COOKIE) ) {
    $phpjs->credentials = json_decode(base64_decode($_COOKIE['pk_credentials']));
    $phpjs->credentials->oauth_code = $_GET['code'];
    $phpjs->api_host = $_COOKIE['pk_api_host'];
    unset($_COOKIE['pk_credentials']);
    unset($_COOKIE['pk_api_host']);
} else if(isset($_REQUEST['lti_message_type'])) {
    if( $devel ) {
        $role = $app_config['app']['test_role'];
        $real_role = $app_config['app']['test_role'];
    } else if( $_REQUEST['roles' ] == 'Instructor' || (array_key_exists('roles_ext', $_REQUEST) && strpos($_REQUEST['roles_ext'], 'Instructor')) ) {
        $r = explode('/', explode(',', $_REQUEST['roles'])[0]);
        $real_role = array_pop($r);
        $role = "instructor";
    } else if( strpos($_REQUEST['roles'], 'TeachingAssistant') || (array_key_exists('roles_ext', $_REQUEST) && strpos($_REQUEST['roles_ext'], 'TeachingAssistant')) ) {
        $r = explode('/', explode(',', $_REQUEST['roles'])[0]);
        $real_role = array_pop($r);
        $role = "ta";
    } else if( strpos($_REQUEST['roles'], 'Administrator') ) {
        $role = "admin";
        $real_role = "admin";
    }
    $secret_file = fopen("creds/.lti-secret", "r") or die("Unable to perform LTI authentication!");
    $secret = trim(fread($secret_file,filesize("creds/.lti-secret")));
    fclose($secret_file);

    $context = new BLTI($secret, true, false);

    if($context->complete) exit(); //True if redirect was done by BLTI class
    if($context->valid) { //True if LTI request was verified
        $user = $_REQUEST['lis_person_sourcedid']; //Canvas User ID param
        $api_host = $_REQUEST['custom_canvas_api_domain'];
    } else {
        die("LTI Authentication Error");
    }
} else if( array_key_exists('test_soft', $app_config['app']) && $app_config['app']['test_soft'] ) {
    $soft_auth = true;
} else if( $devel ) {
    if( array_key_exists('user', $_GET) )
    {
        $user = $_GET['user'];
    } else {
        $user = $app_config['app']['test_user'];
    }
    $api_host = $app_config['app']['canvas_host'];
} else if( array_key_exists('auth', $app_config) && array_key_exists('soft', $app_config['auth'])) {
    $soft_auth = $app_config['auth']['soft'];
} else {
    die("Must authenticate with LTI.");
}

if( $soft_auth ) {
    $phpjs->credentials->active = 'no';

} else if( !array_key_exists('post_oa', $_GET) ){

    // Generate a token for the Websocket Server
    $secret_file = fopen("creds/.secret", "r") or die("Unable to generate access token!");
    $secret = trim(fread($secret_file,filesize("creds/.secret")));
    fclose($secret_file);

    $now = time() - 30;
    $valid_for = 60 * 60 * 60; //60 Minutes
    $valid_until = $now + $valid_for;

    $unencrypted_string = $user.$secret.$api_host.$now.$valid_until;
    $token_t = md5($unencrypted_string);

    $timed_token = $token_t."-".$now."-".$valid_until;
    $phpjs->credentials->user = $user;
    $phpjs->credentials->token = $timed_token;
    $phpjs->credentials->role = $role;
    $phpjs->credentials->active = 'yes';
    $phpjs->credentials->email = $_REQUEST['lis_person_contact_email_primary'];
    $phpjs->credentials->uid = $_REQUEST['custom_canvas_user_id'];
    $phpjs->credentials->real_role = $real_role;
    setcookie('pk_credentials', base64_encode(json_encode($phpjs->credentials)));

    $phpjs->api_host = $api_host;
    setcookie('pk_api_host', $api_host);
}

?>
