<!DOCTYPE html>
<html ng-app="presto-app" ng-init="">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title><?php echo $title;?></title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!--<script src="https://dmc1acwvwny3.cloudfront.net/atatus.js"> </script>
  <script type="text/javascript"> atatus.config('276abfd39d094143877d5bd08b576c42', {customData: <?php echo json_encode($phpjs)?>}).install(); </script>-->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">

  <!-- VIS -->
  <script type="text/javascript" src="bower_components/vis/dist/vis.js"></script>
  <link href="bower_components/vis/dist/vis.css" rel="stylesheet" type="text/css"/>
  <!-- JQUERY -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
  <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- BOOTSTRAP -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/nu-bootstrap.css">
  <link rel="stylesheet" href="apps/<?php echo $app->id;?>/css/styles.css">
  <link rel="stylesheet" href="apps/<?php echo $app->id;?>/css/legend.css">
  <link rel="stylesheet" href="apps/<?php echo $app->id;?>/css/jplot.css">
  <script src="js/lib/bootstrap.min.js"></script>
  <!-- ANGULAR -->
  <!--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular.js"></script>-->
  <script type="text/javascript" src="bower_components/angular/angular.min.js"></script>
  <!--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular-cookies.js"></script>-->
  <script type="text/javascript" src="bower_components/angular-cookies/angular-cookies.min.js"></script>
  <script src="bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
  <!--<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.1.js"></script>-->
  <!--
  <script src="js/pk-core/presto.js"></script>
  <script src="js/pk-core/presto-hook-manager.js"></script>
  <script src="js/pk-core/presto-variable-manager.js"></script>
  <script src="js/pk-core/presto-websocket-manager.js"></script>
  <script src="js/pk-core/prestowebsocketmock.js"></script>
  <script src="apps/<?php echo $app->id;?>/js/app.js"></script>
  <script src="apps/<?php echo $app->id;?>/js/retrieve_data.js"></script>
  <script src="apps/<?php echo $app->id;?>/js/process_data.js"></script>
  -->
  <!--<script src="bower_components/angular-smart-table/dist/smart-table.debug.js"></script>-->
  <!-- D3 -->
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.4.13/d3.min.js"></script>-->
  <!-- GOOGLE -->


  <!-- MISC -->
  <script src="bower_components/uri.js/src/URI.min.js"></script>
  <script type="text/javascript" src="js/lib/papaparse.js"></script>
  <?php
  /*
    $file = getFileLink('params.js', '');
    if( !$file ) {
      $file = "apps/".$app->id."/js-nocompress/params.js";
    }
    $jsinclude = "<script src='".$file."'></script>";
    echo $jsinclude;
    */
  ?>
  <script src="apps/<?php echo $app->id;?>/js-nocompress/params.js"></script>
  <script src="apps/<?php echo $app->id;?>/script.js"></script>
<link href="https://du11hjcvx0uqb.cloudfront.net/dist/brandable_css/ad711c6cf051c64652b8da7ba2240b46/new_styles_normal_contrast/bundles/tinymce-95b5e0c220.css" media="all" rel="stylesheet" type="text/css" />
  <script src="//tinymce.cachefly.net/4.3/tinymce.min.js"></script>
</head>

<body>
<?php
$phpjs->other = new StdClass();
$phpjs->course_id = $_REQUEST['custom_canvas_course_id'];
//$phpjs->api_host = $_REQUEST['custom_canvas_api_domain'];
if( $devel ) {
  $phpjs->credentials->user = $app_config['app']['test_user'];
  $phpjs->credentials->uid = $app_config['app']['test_uid'];
}
?>

  <section class="page" ng-controller="PrestoApp as c" ng-init='php(<?php echo json_encode($phpjs)?>)'>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" rel="home" href="#"><?php echo $title;?></a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li role="presentation" ng-class="{active: tab==1}"><a href="#" ng-click="selectTab(1)">Discussions</a></li>
          <!--<li role="presentation" ng-class="{active: tab==2}"><a href="#" ng-click="selectTab(2)">Peer Reviews</a></li>-->
           {{corenav}}
        </ul>
        <div class="pull-right">
          <p class="navbar-text" ng-show="graph_settings.group.enabled"></p>

        </div>
      </div>
    </div>

    <div id="main-panel" class="container-fluid">

    <div class="row">
      <div class="alert alert-danger" ng-show="error" role="alert">({{error_count}}) {{error_message}}</div>

      <div class="presto-panel panel panel-primary" ng-show="tab == 1" ng-hide="progress.percent != 0">
        <div class="panel-heading">Discussions Analysis</div>
        <div ng-show="credentials.role == 'admin'" ng-hide="go">
          <h3>Choose a course</h3>
          <div class="course-id input-group">
            <input type="text" class="form-control" ng-model="course_id"></input>
            <span class="input-group-btn">
              <button type="button" class="btn btn-primary form-control" ng-click="hook('init')">Load Course ID</button>
            </span>
          </div>
        </div>

      </div>
        <!--
        <h3>1. Choose a Group</h3>
        <div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{group_name || "Choose Group"}} <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" ng-hide="graph_settings.group.enabled">
                <li ng-repeat="group in group_list"><a href="#" ng-click="set_group(group)">{{group.name}}</a></li>
                <li class="divider"></li>
                <li><a href="#" ng-click="set_group(0)">None</a></li>
          </ul>
        </div>
        <hr/>
        <span ng-show="show_topics">
        <h3>2. Choose Topics</h3>
        <div class="btn-group">
          <button type="button" class="btn btn-default" ng-click="selectAllTopics()">Select All</button>
          <button type="button" class="btn btn-default" ng-click="selectNoTopics()">Select None</button>
        </div>
        <br/>
        <br/>
        <div class="well scroll-list btn-group-vertical">
            <input type="checkbox" id="{{topic.id}}" data-toggle="button" ng-model="topics[topic.id].selected">
              <label ng-disabled="topics[topic.id].disabled" class="topic btn btn-default" ng-model="topics[topic.id].selected" btn-checkbox ng-repeat="topic in topics">{{topic.text}}</label>
        </div>
        <h3>3. Choose Analysis Types</h3>
        <hr/>
        <div class="btn-group">
              <label class="btn btn-default" ng-model="analysis_types[type.id].selected" btn-checkbox ng-repeat="type in analysis_types">{{type.text}}</label>
        </div>
        <hr>
        <h3>4. Start Analysis</h3>
        <div class="btn-group">
          <button type="button" class="btn btn-primary" ng-click="startExtraction()" ng-disabled="!not_extracting">Start Extraction</button>
          <button type="button" class="btn btn-primary" ng-click="buildCsv()" ng-disabled="not_extracting || progress.percent != 100">Download CSV</button>
          <button type="button" class="btn btn-primary" ng-click="buildAggCsv()" ng-disabled="not_extracting || progress.percent != 100">Download Aggregated CSV</button>
        </div>
        </span>-->

      <div class="presto-panel panel panel-primary" ng-show="tab == 1 && progress.percent == 100">
        <div class="panel-heading">Data Export</div>
        <div ng-show="progress.percent == 100" class="buttons">
          <button type="button" class="btn btn-primary" ng-click="buildCsv()">Download CSV</button>
          <button type="button" class="btn btn-primary" ng-click="buildAggCsv()">Download Aggregated CSV</button>
        </div>
      </div>
      <div class="presto-panel panel panel-primary" ng-show="tab == 1 && go">
        <div class="panel-heading">Word Cloud</div>
        <div  ng-hide="progress.percent == 100 || hide_progress || progress.percent == 0">
          <h3>Loading Discussion Data...</h3>
          <div class="progress" style="margin: 7px 7px 7px 7px;">
            <div class="progress-bar" role="progressbar" aria-valuenow="{{progress.percent}}" aria-valuemin="0" aria-valuemax="100" style="width: {{progress.percent}}%;">
              {{progress.percent}}%
            </div>
          </div>
        </div>
        <div class="wordcloud">
          <pk-cloud words="words" on-click="test(word)"></pk-cloud>
        </div>
      </div>
      <a name="posts"></a>
      <div class="presto-panel panel panel-primary posts" ng-show="tab == 1 && active.length > 0">
        <div class="panel-heading">Posts containing the {{word.type}} of "{{word.word}}":</div>
        <forum-post data="row" ng-repeat="row in active" participants="participants"></forum-post>
      </div>
      <!--</div>-->
      <div class="presto-panel panel panel-primary" ng-show="tab == 2">
        <div class="panel-heading">Peer Reviews Analysis</div>
            <h2 ng-hide="done"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" ng-hide="done"></span> Please wait...</h2>
            <h3 ng-hide="done">This process may take several minutes depending on Canvas performance.</h3>
            <div class="pad-20 dropdown" ng-show="done">
              <br />
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                {{dd_text}}
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu center" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation" ng-repeat="assignment_obj in assignments_list"><a  role="menuitem" tabindex="-1" ng-click="getAssignments(assignment_obj.id, assignment_obj.name)">{{assignment_obj.name}}</a></li>
              </ul>
            </div>
            <table st-table="comments" class="table table-striped" ng-show="done && assignment">

              <thead>
                <tr>
                  <th st-sort="netid">NetID</th>
                  <th st-sort="user_name">Name</th>
                  <th>Review 1</th>
                  <th>Review 2</th>
                  <th>Review 3</th>
                  <th>Review 4</th>
                  <th>Review 5</th>
                  <th>Review 6</th>
                </tr>
                <tr>
                  <th colspan=8>
                    <div class="progress" style="margin: 7px;" ng-hide="prog_perc == 100">
                      <div class="progress-bar" role="progressbar" aria-valuenow="{{prog_perc}}" aria-valuemin="0" aria-valuemax="100" style="width: {{prog_perc}}%;">
                        {{prog_perc}}%
                      </div>
                    </div>
                    <form class="form-inline row" role="search" ng-show="prog_perc == 100">
                        <div class="form-group col-xs-10">
                            <input st-search placeholder="Search users..." class="search-box form-control" type="search"/>
                        </div>
                        <div class="form-group col-xs-2">
                            <csv data="comments_csv" filename="assignment" ng-show="csv_done"></csv>
                        </div>
                    </form>
                    <!--
                    <div class="col-xs-10" ng-show="prog_perc == 100">
                        <input st-search placeholder="Search users..." class="search-box form-control" type="search"/>
                    </div>
                    <div class="col-xs-2" ng-show="prog_perc == 100">
                      <csv data="comments_csv" filename="'dd_text'"></csv>
                    </div>-->
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="(review_index, row) in comments">
                  <td>{{row[0].author_netid}}</td>
                  <td>{{row[0].author_name}}</td>
                  <td ng-repeat="row_index in [0,1,2,3,4,5]" class="st-left">
                    <strong ng-show="row[row_index].reviewed_user.name">To: <span class="hide-text">{{row[row_index].reviewed_user.name}}</span><br/>{{row[row_index].created_at | date:'MMM d, y h:mma'}}<hr class="hr-thin"/></strong> <span class="hide-text">{{row[row_index].comment}}</span> <keywords ng-show="row[row_index].reviewed_user.name" review-index="{{review_index}}" row-index="{{row_index}}"></keywords>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
      </div>
    </div>
   </div>
 </section>
</body>

</html>
