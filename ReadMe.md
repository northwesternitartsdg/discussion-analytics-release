# 1. Requirements
1. Server running Apache/PHP at `yourhostname.edu`
2. Canvas Instance at `yourorganization.instructure.com`

# 2. Installation
## 2.a Preparing Canvas for OAuth
Follow the Instructure guide for adding Developer Keys here: [How do I add a developer key for an account?](https://guides.instructure.com/m/4214/l/441833-how-do-i-add-a-developer-key-for-an-account)

1. For Key Name enter `Discussion Analytics`
2. For Owner Email enter your admin email address
3. For Redirect URI (Legacy) *leave blank*
4. For Redirect URIs enter `https://yourhostname.edu/pk-app/index.php`
5. Click Save Key, note the values for `Key` and `ID`

## 2.b.a Install Discussion Analytics to your web server

1. In BitBucket, navigate to Downloads > Download Repository
2. Unzip the archive downloaded
3. Make a directory in your web root called "pk-app" that maps to `https://yourhostname.edu/pk-app`
4. Unzip the contents of the archive into this directory

## 2.b.b Configure Discussion Analytics

5. Open `<web-root>/pk-app/apps/pk-discussion-cloud/app.example.ini` in your preferred text editor.
6. Add the value from 1.2 to canvas_host

        #EXAMPLE 2.b.2
        [app]
        	title = Discussion Analytics
        	canvas_host = yourorganization.instructure.com
        	
        [pk-include]
        	auth = true
        	oauth = true
        [oauth]
        	client_id =
        	client_secret =
        	redirect_uri =

7. Add `ID` from 2.a.5 to `client_id`

        #EXAMPLE 2.b.3
        [app]
        	title = Discussion Analytics
        	canvas_host = yourorganization.instructure.com
        	
        [pk-include]
        	auth = true
        	oauth = true
        [oauth]
        	client_id = 1087000000000000
        	client_secret =
        	redirect_uri =

8. Add `Key` from 2.a.5 to `client_secret`

        #EXAMPLE 2.b.4
        [app]
        	title = Discussion Analytics
        	canvas_host = yourorganization.instructure.com
        	
        [pk-include]
        	auth = true
        	oauth = true
        [oauth]
        	client_id = 1087000000000000
        	client_secret = XXXXXXXXXXXXXXXXXXXXXXXXX
        	redirect_uri =

9. Add `Redirect URI` from 2.a.4 to `redirect_uri`

        #EXAMPLE 2.b.5
        [app]
        	title = Discussion Analytics
        	canvas_host = yourorganization.instructure.com
        	
        [pk-include]
        	auth = true
        	oauth = true
        [oauth]
        	client_id = 1087000000000000
        	client_secret = XXXXXXXXXXXXXXXXXXXXXXXXX
        	redirect_uri =

10. Save this file as `app.ini` in the same directory

## 2.b.c Configure LTI

1. Create a file in `<web-root>/pk-app/creds` with the name `.lti-secret`
2. Add a `shared secret string` to the file as a single line, it should be very random and complex. This will be entered into Canvas upon installation into a course/account.
3. Create another file in `<web-root>/pk-app/creds` with the name `.secret`
4. Add another random and complex string into this file as a single line. This is only used internally and does not need to be noted.

## 2.b.d Add to Course/Account
1. Follow instructions from Instructure on adding an app for an account here: [How do I manually configure an external app for an account?](https://guides.instructure.com/m/4214/l/74561-how-do-i-manually-configure-an-external-app-for-an-account)
2. The final configuration should look something like this:
![LTI Config](https://bitbucket.org/repo/Kkb8ke/images/2341613344-lti_config.png)

# 3. License
Apache 2.0 License. See the [LICENSE.txt](https://bitbucket.org/northwesternitartsdg/discussion-analytics-release/src/master/LICENSE.txt) file.